import {createApp} from 'vue'
import App from './App.vue'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

import store from './store'

const app = createApp(App)

app.use(Buefy)
app.use(store)

app.mount('#app')
