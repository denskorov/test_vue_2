import {createStore} from 'vuex'

const store = createStore({

    state: () => ({
        tasks: [
            {
                id: 1,
                text: ';qwertyukiot',
                created: Date.now(),
                completed: true
            }
        ],
    }),

    mutations: {
        setTasks: (state, tasks) => state.tasks = tasks,
        setCompletedTasks(state, {id, completed}) {
            console.log(id, completed)
            const task = state.tasks.find(task => task.id === id)
            task.completed = completed
        }
    },

    getters: {},

    actions: {
        saveToLocaleStore({state}) {
            localStorage.setItem('tasks', JSON.stringify(state.tasks))
        }
    }
})

export default store